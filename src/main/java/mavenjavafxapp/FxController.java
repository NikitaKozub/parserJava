package mavenjavafxapp;

import Pars.Parser;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.awt.*;
import java.awt.TextField;
import java.io.IOException;
import java.util.List;

/**
 * Created by Никита on 18.07.2017.
 */
public class FxController {
    public javafx.scene.control.TextField url;
    public javafx.scene.control.TextField tag;
    public javafx.scene.control.ListView listSrc;
    public javafx.scene.control.TextField tagName;
    Parser parser;


    public void setTextParser(Parser parser){
        List<String> srcList;
        srcList= parser.getListSrcImage();
        ObservableList<String> oListStavaka = FXCollections.observableArrayList(srcList);
        listSrc.setItems(oListStavaka);


    }

    @FXML
    public void onClickMethod(ActionEvent actionEvent) throws IOException {
        //System.out.print(url.getText());
        parser = new Parser();
        //parser.parser(url.getText(),tag.getText());
        parser.parserTagName(url.getText(),tagName.getText());
        setTextParser(parser);
    }
}
