package Pars;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by Никита on 17.07.2017.
 */
public class Parser {
    public List<String> listSrcImage;
    Document doc;
    public List<String> getListSrcImage() {
        return this.listSrcImage;
    }

    public void parserTagName(String url,String tag) throws IOException {
        listSrcImage=new ArrayList<String>();
        doc = Jsoup.connect(url).get();
        Elements links = doc.getElementsByTag(tag);

        for (Element link : links) {
            listSrcImage.add(links.toString());
            System.out.print(listSrcImage.get(0));
        }
    }

    public void parser(String url,String tag) throws IOException {
        listSrcImage=new ArrayList<String>();
        doc = Jsoup.connect(url).get();
        Elements links = doc.select("a[href]");
        Elements media = doc.select("["+tag+"]");
        Elements imports = doc.select("link[href]");

        print("\nMedia: (%d)", media.size());
        for (Element src : media) {
            if (src.tagName().equals("img")){
                listSrcImage.add(src.attr(tag).toString());
            }
            else{
                listSrcImage.add(src.attr(tag).toString());
            }

        }

        print("\nImports: (%d)", imports.size());
        for (Element link : imports) {
            print(" * %s <%s> (%s)", link.tagName(),link.attr("abs:href"), link.attr("rel"));
        }

        print("\nLinks: (%d)", links.size());
        for (Element link : links) {
            print(" * a: <%s>  (%s)", link.attr("abs:href"), trim(link.text(), 35));
        }
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;
    }
}
